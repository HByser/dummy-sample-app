package com.project.post.detailedPost.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.eXalt.post.R
import com.project.post.detailedPost.viewobjects.DetailedPostCommentVO

class DetailedPostCommentListAdapter(
    private val context: Context
) : ListAdapter<DetailedPostCommentVO, DetailedPostCommentListAdapter.DetailedPostCommentViewHolder>(PostDiffCallBack()) {

    var onUserClick: ((String) -> Unit)? = null

    inner class DetailedPostCommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(comment: DetailedPostCommentVO) {
            itemView.findViewById<ImageView>(R.id.comment_profile_image).let {
                Glide.with(context)
                    .load(comment.ownerPictureUri)
                    .circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(it)
                it.setOnClickListener {
                    onUserClick?.invoke(comment.ownerId)
                }
            }
            itemView.findViewById<TextView>(R.id.comment_username).text = comment.owner
            itemView.findViewById<TextView>(R.id.comment_text).text = comment.message
            itemView.findViewById<TextView>(R.id.comment_publish_date).text = comment.publishDate
        }
    }

    private class PostDiffCallBack : DiffUtil.ItemCallback<DetailedPostCommentVO>() {
        override fun areItemsTheSame(oldItem: DetailedPostCommentVO, newItem: DetailedPostCommentVO) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: DetailedPostCommentVO, newItem: DetailedPostCommentVO) =
                    oldItem.owner == newItem.owner &&
                    oldItem.publishDate == newItem.publishDate &&
                    oldItem.message == newItem.message
    }

    override fun onBindViewHolder(holder: DetailedPostCommentViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailedPostCommentViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.comment_item, parent, false)
        return DetailedPostCommentViewHolder(itemView)
    }
}