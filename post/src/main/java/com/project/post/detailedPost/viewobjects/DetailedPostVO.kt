package com.project.post.detailedPost.viewobjects

data class DetailedPostVO(
    val id: String?,
    val text: String,
    val image: String,
    val publishDate: String,
    val ownerId: String,
    val ownerName: String,
    val tags: List<String>,
    val ownerPictureUri: String,
    val likes: Int,
)