package com.project.post.detailedPost.mappers

import com.exalt.domain.post.models.DetailedPostModel
import com.project.post.detailedPost.viewobjects.DetailedPostVO
import javax.inject.Inject

class DetailedPostMapper @Inject constructor() {
    fun toDetailedPostVO(detailedPostModel: DetailedPostModel) = DetailedPostVO(
        id = detailedPostModel.id,
        image = detailedPostModel.image,
        text = detailedPostModel.text,
        likes = detailedPostModel.likes,
        ownerId = detailedPostModel.owner.id,
        ownerName = detailedPostModel.owner.name,
        tags = detailedPostModel.tags,
        ownerPictureUri = detailedPostModel.owner.pictureUrl,
        publishDate = detailedPostModel.publishDate.split("T")[0],
    )
}