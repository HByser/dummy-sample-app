package com.project.post.detailedPost.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.eXalt.post.R
import com.eXalt.post.databinding.FragmentDetailedPostBinding
import com.google.android.material.chip.Chip
import com.project.post.detailedPost.adapters.DetailedPostCommentListAdapter
import com.project.post.detailedPost.extensions.handleVisibility
import com.project.post.detailedPost.viewmodels.DetailedPostViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailedPostFragment : Fragment(R.layout.fragment_detailed_post) {
    private val detailedPostViewModel: DetailedPostViewModel by viewModels()
    private var _binding: FragmentDetailedPostBinding? = null
    private val binding get() = _binding!!
    var onUserClick: ((String) -> Unit)? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailedPostBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?,
    ) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initDetailedPostObservers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() {
        binding.commentRecyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            adapter =
                DetailedPostCommentListAdapter(this@DetailedPostFragment.requireContext()).apply {
                    onUserClick = { userId ->
                        val request = NavDeepLinkRequest.Builder
                            .fromUri("chienstagram://user/$userId".toUri())
                            .build()
                        findNavController().navigate(request)
                    }
                }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initDetailedPostObservers() {
        detailedPostViewModel.isLoading.observe(viewLifecycleOwner) {
            requireView().findViewById<ProgressBar>(R.id.progress_post).handleVisibility(it)
        }
        detailedPostViewModel.detailedPost.observe(viewLifecycleOwner) { detailedPostView ->
            detailedPostView.tags.forEach { tag ->
                val chip = Chip(context)
                chip.text = tag
                binding.detailedPostContainer.tagChipGroup.addView(chip)
            }
            binding.detailedPostContainer.likesChip.text =
                "${detailedPostViewModel.detailedPost.value?.likes.toString()} likes"
            binding.detailedPostContainer.userDetailedPostUsername.text =
                detailedPostViewModel.detailedPost.value?.ownerName
            binding.detailedPostContainer.postDescription.text =
                detailedPostViewModel.detailedPost.value?.text
            binding.detailedPostContainer.userDetailedPostDate.text =
                detailedPostViewModel.detailedPost.value?.publishDate
            binding.detailedPostContainer.profileImage.let {
                Glide.with(requireContext())
                    .load(detailedPostView.ownerPictureUri)
                    .circleCrop()
                    .into(it)
                binding.detailedPostContainer.animalPick.let {
                    Glide.with(requireContext())
                        .load(detailedPostView.image)
                        .into(it)
                }
            }
        }
        detailedPostViewModel.detailedPostComment.observe(viewLifecycleOwner) {
            (binding.commentRecyclerview.adapter as DetailedPostCommentListAdapter).submitList(
                it
            )
        }
    }
}