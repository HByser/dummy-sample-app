package com.project.post.detailedPost.mappers

import com.exalt.domain.post.models.DetailedPostCommentModel
import com.project.post.detailedPost.viewobjects.DetailedPostCommentVO
import javax.inject.Inject

class DetailedPostCommentMapper @Inject constructor() {
    fun toListDetailedPostCommentVO(detailedPostCommentModels: List<DetailedPostCommentModel>) =
        detailedPostCommentModels.map { toDetailedPostCommentVO(it) }

    private fun toDetailedPostCommentVO(detailedPostCommentModel: DetailedPostCommentModel) = DetailedPostCommentVO(
        id = detailedPostCommentModel.id,
        ownerId = detailedPostCommentModel.owner.id,
        message = detailedPostCommentModel.message,
        owner = detailedPostCommentModel.owner.name,
        publishDate = detailedPostCommentModel.publishDate.split("T")[0],
        ownerPictureUri = detailedPostCommentModel.owner.pictureUrl,
    )
}