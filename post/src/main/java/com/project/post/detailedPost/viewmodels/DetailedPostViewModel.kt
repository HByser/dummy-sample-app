package com.project.post.detailedPost.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.exalt.domain.post.usecases.GetDetailedPostCommentUseCase
import com.exalt.domain.post.usecases.GetDetailedPostUseCase
import com.project.post.detailedPost.mappers.DetailedPostCommentMapper
import com.project.post.detailedPost.mappers.DetailedPostMapper
import com.project.post.detailedPost.viewobjects.DetailedPostCommentVO
import com.project.post.detailedPost.viewobjects.DetailedPostVO
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailedPostViewModel @Inject constructor(
    private val getDetailedPostUseCase: GetDetailedPostUseCase,
    private val detailedPostMapper: DetailedPostMapper,
    private val getDetailedPostCommentUseCase: GetDetailedPostCommentUseCase,
    private val detailedPostCommentMapper: DetailedPostCommentMapper,
    private val stateHandle: SavedStateHandle,
) : ViewModel() {
    private val postId = stateHandle.get<String>("postId")
    private val _isLoading = MutableLiveData(true)
    val isLoading: LiveData<Boolean> = _isLoading
    private val isError = MutableLiveData(false)

    val detailedPost: LiveData<DetailedPostVO> = liveData {
        if (postId != null) {
            val post = getDetailedPostUseCase.invoke(postId)
            val detailedPostVo = detailedPostMapper.toDetailedPostVO(post)
            emit(detailedPostVo)
        } else {
            isError.value = true
            _isLoading.value = false
        }
    }

    val detailedPostComment: LiveData<List<DetailedPostCommentVO>> = liveData {
        if (postId != null) {
            emit(
                detailedPostCommentMapper.toListDetailedPostCommentVO(
                    getDetailedPostCommentUseCase.invoke(postId)
                )
            )
            _isLoading.value = false
        } else {
            isError.value = true
        }
    }
}