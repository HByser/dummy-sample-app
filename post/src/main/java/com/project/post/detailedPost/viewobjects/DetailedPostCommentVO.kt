package com.project.post.detailedPost.viewobjects

data class DetailedPostCommentVO(
    val id: String,
    val owner: String,
    val ownerId: String,
    val message: String,
    val publishDate: String,
    val ownerPictureUri: String,
)