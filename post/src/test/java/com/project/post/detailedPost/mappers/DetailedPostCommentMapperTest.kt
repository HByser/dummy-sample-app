package com.project.post.detailedPost.mappers

import com.exalt.domain.home.models.DomainModelFactory.getDefaultDetailedPostCommentModel
import com.exalt.domain.post.models.DetailedPostCommentModel
import com.project.post.DetailedPostCommentVOFactory.getDefaultDetailedPostCommentVO
import java.util.UUID
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test

class DetailedPostCommentMapperTest {
    private val detailedPostCommentMapper = DetailedPostCommentMapper()

    @Test
    fun `Given list of Detailed Post Comment Model, when mapper is called, Then returns list of DetailedPostCommentVO`() {
        //Given
        val randomUuids = List(23) { UUID.randomUUID().toString() }

        //When
        val actualDetailedPostCommentVO = detailedPostCommentMapper.toListDetailedPostCommentVO(randomUuids.map { getDefaultDetailedPostCommentModel(it) })
        val expectedDetailedPostCommentVO = randomUuids.map { getDefaultDetailedPostCommentVO(it) }

        //Then
        assertEquals(expectedDetailedPostCommentVO, actualDetailedPostCommentVO)
    }

    @Test
    fun `Given empty list of Detailed Post Comment Model, when mapper is called, Then returns empty list of DetailedPostCommentVO`() {
        //Given
        val detailedPostCommentModels = emptyList<DetailedPostCommentModel>()

        //When
        val actualDetailedPostCommentVO = detailedPostCommentMapper.toListDetailedPostCommentVO(detailedPostCommentModels)

        //Then
        assertTrue(actualDetailedPostCommentVO.isEmpty())
    }
}