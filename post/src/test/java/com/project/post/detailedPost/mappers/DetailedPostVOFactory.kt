package com.project.post.detailedPost.mappers

import com.exalt.domain.home.models.DomainModelFactory.DETAILED_POST_ID
import com.exalt.domain.home.models.DomainModelFactory.DETAILED_POST_IMAGE
import com.exalt.domain.home.models.DomainModelFactory.DETAILED_POST_LIKES
import com.exalt.domain.home.models.DomainModelFactory.DETAILED_POST_PUBLISH_DATE
import com.exalt.domain.home.models.DomainModelFactory.DETAILED_POST_TAGS
import com.exalt.domain.home.models.DomainModelFactory.DETAILED_POST_TEXT
import com.exalt.domain.home.models.DomainModelFactory.OWNER_FIRST_NAME
import com.exalt.domain.home.models.DomainModelFactory.OWNER_ID
import com.exalt.domain.home.models.DomainModelFactory.OWNER_LAST_NAME
import com.exalt.domain.home.models.DomainModelFactory.OWNER_PICTURE_URL
import com.project.post.detailedPost.viewobjects.DetailedPostVO

object DetailedPostVOFactory {
    fun getDefaultPostVO(postId: String = DETAILED_POST_ID) = DetailedPostVO(
        id = postId,
        text = DETAILED_POST_TEXT,
        image = DETAILED_POST_IMAGE,
        publishDate = DETAILED_POST_PUBLISH_DATE,
        ownerId = OWNER_ID,
        ownerName = "$OWNER_FIRST_NAME $OWNER_LAST_NAME",
        tags = DETAILED_POST_TAGS,
        ownerPictureUri = OWNER_PICTURE_URL,
        likes = DETAILED_POST_LIKES,
    )
}