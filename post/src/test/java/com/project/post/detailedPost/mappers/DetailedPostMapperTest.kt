package com.project.post.detailedPost.mappers

import com.exalt.domain.home.models.DomainModelFactory.getDetailedPostModel
import com.project.post.detailedPost.mappers.DetailedPostVOFactory.getDefaultPostVO
import junit.framework.TestCase.assertEquals
import org.junit.Test

class DetailedPostMapperTest {
    private val detailedPostMapper = DetailedPostMapper()

    @Test
    fun `Given a detailedPost, when mapper is called, Then returns DetailedPostVO`() {
        val actualDetailedPostVO = detailedPostMapper.toDetailedPostVO(getDetailedPostModel())
        val expectedDetailedPostVO = getDefaultPostVO()

        assertEquals(expectedDetailedPostVO, actualDetailedPostVO)
    }
}