package com.project.post

import com.exalt.domain.home.models.DomainModelFactory.OWNER_FIRST_NAME
import com.exalt.domain.home.models.DomainModelFactory.OWNER_ID
import com.exalt.domain.home.models.DomainModelFactory.OWNER_LAST_NAME
import com.exalt.domain.home.models.DomainModelFactory.OWNER_PICTURE_URL
import com.exalt.domain.home.models.DomainModelFactory.POST_COMMENT_ID
import com.exalt.domain.home.models.DomainModelFactory.POST_COMMENT_MESSAGE
import com.exalt.domain.home.models.DomainModelFactory.POST_COMMENT_PUBLISH
import com.project.post.detailedPost.viewobjects.DetailedPostCommentVO

object DetailedPostCommentVOFactory {
    fun getDefaultDetailedPostCommentVO(postId: String = POST_COMMENT_ID) = DetailedPostCommentVO(
        id = postId,
        owner = "$OWNER_FIRST_NAME $OWNER_LAST_NAME",
        message = POST_COMMENT_MESSAGE,
        publishDate = POST_COMMENT_PUBLISH,
        ownerPictureUri = OWNER_PICTURE_URL,
        ownerId = OWNER_ID,
    )
}