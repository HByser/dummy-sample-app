package com.exalt.user.profile.mappers

import com.exalt.domain.home.models.DomainModelFactory.getDefaultProfileModel
import com.exalt.user.ProfileVOFactory.getDefaultProfileVO
import org.junit.Assert.assertEquals
import org.junit.Test


class ProfileMapperTest {
    private val profileMapper = ProfileMapper()

    @Test
    fun `Given a profile, when mapper is called, Then returns ProfileVO`() {
        val actualProfileVO = profileMapper.toProfileVO(getDefaultProfileModel())
        val expectedProfileVO = getDefaultProfileVO()

        assertEquals(expectedProfileVO, actualProfileVO)
    }
}