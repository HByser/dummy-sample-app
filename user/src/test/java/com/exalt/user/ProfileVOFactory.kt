package com.exalt.user

import androidx.compose.ui.graphics.Color
import com.exalt.domain.home.models.DomainModelFactory.USER_BIRTH_DATE
import com.exalt.domain.home.models.DomainModelFactory.USER_CITY
import com.exalt.domain.home.models.DomainModelFactory.USER_COUNTRY
import com.exalt.domain.home.models.DomainModelFactory.USER_EMAIL
import com.exalt.domain.home.models.DomainModelFactory.USER_FIRST_NAME
import com.exalt.domain.home.models.DomainModelFactory.USER_GENDER
import com.exalt.domain.home.models.DomainModelFactory.USER_ID
import com.exalt.domain.home.models.DomainModelFactory.USER_LAST_NAME
import com.exalt.domain.home.models.DomainModelFactory.USER_PHONE
import com.exalt.domain.home.models.DomainModelFactory.USER_PICTURE
import com.exalt.domain.home.models.DomainModelFactory.USER_REGISTER_DATE
import com.exalt.domain.home.models.DomainModelFactory.USER_STATE
import com.exalt.domain.home.models.DomainModelFactory.USER_STREET
import com.exalt.domain.home.models.DomainModelFactory.USER_TIMEZONE
import com.exalt.domain.home.models.DomainModelFactory.USER_TITLE
import com.exalt.user.profile.viewobjects.ProfileVO

object ProfileVOFactory {
    fun getDefaultProfileVO(id: String = USER_ID) = ProfileVO(
        id = id,
        title = USER_TITLE,
        firstName = USER_FIRST_NAME,
        lastName = USER_LAST_NAME,
        gender = USER_GENDER,
        email = USER_EMAIL,
        dateOfBirth = USER_BIRTH_DATE,
        registerDate = USER_REGISTER_DATE,
        phone = USER_PHONE,
        picture = USER_PICTURE,
        street = USER_STREET,
        city = USER_CITY,
        state = USER_STATE,
        country = USER_COUNTRY,
        timezone = USER_TIMEZONE,
        borderColor = Color.Black,
        genderIcon = 2131230816,
    )
}