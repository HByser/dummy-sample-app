package com.exalt.user.profile.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.exalt.domain.profile.usecases.GetProfileUseCase
import com.exalt.user.profile.mappers.ProfileMapper
import com.exalt.user.profile.viewobjects.ProfileVO
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val getProfileUseCase: GetProfileUseCase,
    private val profileMapper: ProfileMapper,
    private val stateHandle: SavedStateHandle,
) : ViewModel() {
    private val userId = stateHandle.get<String>("userId")
    private val _isLoading = MutableLiveData(true)
    val isLoading: LiveData<Boolean> = _isLoading
    private val isError = MutableLiveData(false)

    val profileData: LiveData<ProfileVO> = liveData {
        if (userId != null) {
            val profile = getProfileUseCase.invoke(userId)
            val profileVo = profileMapper.toProfileVO(profile)
            emit(profileVo)
            _isLoading.value = false
        } else {
            isError.value = true
        }
    }
}