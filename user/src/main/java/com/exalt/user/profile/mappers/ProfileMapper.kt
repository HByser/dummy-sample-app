package com.exalt.user.profile.mappers

import androidx.compose.ui.graphics.Color
import com.exalt.domain.profile.models.ProfileModel
import com.exalt.user.R
import com.exalt.user.profile.viewobjects.ProfileVO
import javax.inject.Inject

class ProfileMapper @Inject constructor() {
    fun toProfileVO(profileModel: ProfileModel) = ProfileVO(
        id = profileModel.id,
        title = profileModel.title,
        firstName = profileModel.firstName,
        lastName = profileModel.lastName,
        gender = profileModel.gender,
        email = profileModel.email,
        dateOfBirth = profileModel.dateOfBirth.split("T")[0],
        registerDate = profileModel.registerDate,
        phone = profileModel.phone,
        picture = profileModel.picture,
        city = profileModel.location.city,
        country = profileModel.location.country,
        state = profileModel.location.state,
        timezone = profileModel.location.timezone,
        street = profileModel.location.street,
        borderColor = if (profileModel.gender == "male") {
            Color(0xFF0000FF)
        } else if (profileModel.gender == "female") {
            Color(0xFFFFB6C1)
        } else {
            Color.Black
        },
        genderIcon = if (profileModel.gender == "male") {
            R.drawable.baseline_male_24
        } else if (profileModel.gender == "female") {
            R.drawable.baseline_female_24
        } else {
            R.drawable.baseline_transgender_24
        }
    )
}