package com.exalt.user.profile.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun ProfileUserInformation(
    firstName: String,
    lastName: String,
    dateOfBirth: String,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 10.dp, bottom = 10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Row {
            Text(
                text = firstName,
                style = TextStyle(fontWeight = FontWeight.Bold),
                fontSize = NAME_FONT_SIZE,
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text = lastName,
                style = TextStyle(fontWeight = FontWeight.Bold),
                fontSize = NAME_FONT_SIZE,
            )
        }
        Text(
            text = dateOfBirth,
            color = Color.Gray,
            fontSize = TEXT_FONT_SIZE,
        )
        Spacer(modifier = Modifier.height(10.dp))
    }
}

@Composable
@Preview
private fun ProfileUserInformationPreview() {
    ProfileUserInformation(
        firstName = "John",
        lastName = "Doe",
        dateOfBirth = "09-07-1999"
    )
}