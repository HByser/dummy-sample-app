package com.exalt.user.profile.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.exalt.user.R

@Composable
fun ProfileHeader(
    borderColor: Color,
    profilePicture: String,
    genderIcon: Int,
) {
    Box(
        modifier = Modifier.fillMaxWidth(),
    ) {
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .height(240.dp),
            painter = rememberAsyncImagePainter(model = "https://media.istockphoto.com/id/476098860/vector/wonderful-morning-in-the-blue-mountains.jpg?s=612x612&w=0&k=20&c=0nuLvsWKXPReu01RvbXTKIwlUYxOQvoXD_qVBrsapxc="),
            contentDescription = stringResource(R.string.background_image),
        )
        Image(
            modifier = Modifier
                .padding(top = 180.dp)
                .align(Alignment.BottomCenter)
                .clip(CircleShape)
                .border(
                    width = 1.dp,
                    color = borderColor,
                    shape = CircleShape,
                )
                .size(100.dp),
            painter = rememberAsyncImagePainter(model = profilePicture),
            contentDescription = stringResource(R.string.animal_image),
        )
        Icon(
            painterResource(id = genderIcon),
            contentDescription = stringResource(R.string.gender_icon),
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(end = 100.dp),
        )
    }
}

@Composable
@Preview
private fun ProfileHeaderPreview() {
    ProfileHeader(
        borderColor = Color.Black,
        profilePicture = "Picture",
        genderIcon = 1,
    )
}