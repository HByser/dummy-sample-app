package com.exalt.user.profile.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.exalt.user.R

@Composable
fun ContactInformation(
    email: String,
    phone: String,
    location: String,
) {
    Column(
        modifier = Modifier.padding(start = 10.dp),
        verticalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        Text(
            text = stringResource(R.string.contact_information),
            color = Color.Gray,
            fontSize = TEXT_FONT_SIZE,
        )
        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
        )
        ContactInformationComponent(
            title = stringResource(R.string.email),
            information = email,
        )
        ContactInformationComponent(
            title = stringResource(R.string.phone),
            information = phone,
        )
        ContactInformationComponent(
            title = stringResource(R.string.address),
            information = location,
        )
    }
}

@Composable
@Preview
private fun ContactInformationPreview() {
    ContactInformation(
        email = "John.doe@example.com",
        phone = "00000000",
        location = "7 rue de la fontaine",
    )
}