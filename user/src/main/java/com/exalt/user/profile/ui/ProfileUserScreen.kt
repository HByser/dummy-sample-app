package com.exalt.user.profile.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.exalt.user.profile.viewmodels.ProfileViewModel

val NAME_FONT_SIZE = 25.sp
val TEXT_FONT_SIZE = 15.sp

@Composable
fun UserProfileContainer(
    profileViewModel: ProfileViewModel = viewModel(),
) {
    val state by profileViewModel.profileData.observeAsState()
    state?.let { profileState ->
        Column(
            modifier = Modifier.fillMaxWidth(),
        ) {
            ProfileHeader(
                borderColor = profileState.borderColor,
                profilePicture = profileState.picture,
                genderIcon = profileState.genderIcon,
            )
            ProfileUserInformation(
                firstName = profileState.firstName,
                lastName = profileState.lastName,
                dateOfBirth = profileState.dateOfBirth,
            )
            ContactInformation(
                email = profileState.email,
                phone = profileState.phone,
                location = "${profileState.street}, " +
                        "${profileState.city} " +
                        "${profileState.state} " +
                        "${profileState.country} ",
            )
        }
    }
}

@Composable
@Preview(showBackground = true)
private fun UserProfilePreview() {
    UserProfileContainer()
}
