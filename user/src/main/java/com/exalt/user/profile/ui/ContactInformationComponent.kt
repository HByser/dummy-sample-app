package com.exalt.user.profile.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun ContactInformationComponent(
    title: String,
    information: String,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Text(
            text = title,
            color = Color.Gray,
            fontSize = TEXT_FONT_SIZE,
        )
        Text(
            text = information,
            color = Color.Black,
            fontSize = TEXT_FONT_SIZE,
        )
    }
}

@Composable
@Preview(showBackground = true)
private fun ContactInformationComponentPreview(
) {
    ContactInformationComponent(
        title = "Email",
        information = "John.doe@gmail.com",
    )
}