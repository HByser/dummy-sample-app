package com.exalt.user.profile.viewobjects

import androidx.compose.ui.graphics.Color

data class ProfileVO(
    val id: String,
    val title: String,
    val firstName: String,
    val lastName: String,
    val gender: String,
    val email: String,
    val dateOfBirth: String,
    val registerDate: String,
    val phone: String,
    val picture: String,
    val street: String,
    val city: String,
    val state: String,
    val country: String,
    val timezone: String,
    val borderColor: Color,
    val genderIcon: Int,
)