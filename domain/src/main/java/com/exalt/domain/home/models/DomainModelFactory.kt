package com.exalt.domain.home.models

import com.exalt.domain.post.models.DetailedPostCommentModel
import com.exalt.domain.post.models.DetailedPostModel
import com.exalt.domain.profile.models.LocationModel
import com.exalt.domain.profile.models.ProfileModel

object DomainModelFactory {
    const val POST_ID = "POST_ID"
    const val POST_TEXT = "POST_TEXT"
    const val POST_IMAGE_URL = "POST_IMAGE_URL"
    const val POST_PUBLISH_DATE = "POST_PUBLISH_DATE"

    const val DETAILED_POST_ID = "DETAILED_POST_ID"
    const val DETAILED_POST_TEXT = "DETAILED_POST_TEXT"
    const val DETAILED_POST_IMAGE = "DETAILED_POST_IMAGE"
    const val DETAILED_POST_PUBLISH_DATE = "DE_POS_PUBLISH"
    val DETAILED_POST_TAGS = listOf("canin", "pet", "mountain")
    const val DETAILED_POST_LIKES = 8

    const val POST_COMMENT_ID = "POST_COMMENT_ID"
    const val POST_COMMENT_MESSAGE = "POST_COMMENT_MESSAGE"
    const val POST_COMMENT_PUBLISH = "PUBLISH"

    const val USER_ID = "USER_ID"
    const val USER_TITLE = "USER_TITLE"
    const val USER_FIRST_NAME = "USER_FIRST_NAME"
    const val USER_LAST_NAME = "USER_LAST_NAME"
    const val USER_GENDER = "USER_GENDER"
    const val USER_EMAIL = "USER_EMAIL"
    const val USER_BIRTH_DATE = "10-02-22"
    const val USER_REGISTER_DATE = "USER_REGISTER_DATE"
    const val USER_PHONE = "USER_PHONE"
    const val USER_PICTURE = "USER_PICTURE"
    const val USER_CITY = "USER_CITY"
    const val USER_COUNTRY = "USER_COUNTRY"
    const val USER_STATE = "USER_STATE"
    const val USER_TIMEZONE = "USER_TIMEZONE"
    const val USER_STREET = "USER_STREET"

    const val OWNER_ID = "OWNER_ID"
    const val OWNER_TITLE = "OWNER_TITLE"
    const val OWNER_FIRST_NAME = "OWNER_FIRST_NAME"
    const val OWNER_LAST_NAME = "OWNER_LAST_NAME"
    const val OWNER_PICTURE_URL = "OWNER_PICTURE_URL"

    fun getDefaultPostPreviewModel(
        id: String = POST_ID
    ) = PostPreviewModel(
        id = id,
        text = POST_TEXT,
        imageUrl = POST_IMAGE_URL,
        publishDate = POST_PUBLISH_DATE,
        owner = getDefaultOwnerPreviewModel(),
    )

    fun getDefaultOwnerPreviewModel() = OwnerPreviewModel(
        id = OWNER_ID, name = "$OWNER_FIRST_NAME $OWNER_LAST_NAME", pictureUrl = OWNER_PICTURE_URL
    )

    fun getUserLocationModel() = LocationModel(
        street = USER_STREET,
        city = USER_CITY,
        country = USER_COUNTRY,
        timezone = USER_TIMEZONE,
        state = USER_STATE,
    )

    fun getDetailedPostModel(
        id: String = DETAILED_POST_ID,
    ) = DetailedPostModel(
        id = id,
        text = DETAILED_POST_TEXT,
        image = DETAILED_POST_IMAGE,
        likes = DETAILED_POST_LIKES,
        tags = DETAILED_POST_TAGS,
        publishDate = DETAILED_POST_PUBLISH_DATE,
        owner = getDefaultOwnerPreviewModel(),
    )

    fun getDefaultDetailedPostCommentModel(
        id: String = POST_ID,
    ) = DetailedPostCommentModel(
        id = id,
        message = POST_COMMENT_MESSAGE,
        publishDate = POST_COMMENT_PUBLISH,
        owner = getDefaultOwnerPreviewModel(),
    )

    fun getDefaultProfileModel(
        id: String = USER_ID,
    ) = ProfileModel(
        id = id,
        title = USER_TITLE,
        firstName = USER_FIRST_NAME,
        lastName = USER_LAST_NAME,
        gender = USER_GENDER,
        email = USER_EMAIL,
        dateOfBirth = USER_BIRTH_DATE,
        registerDate = USER_REGISTER_DATE,
        phone = USER_PHONE,
        picture = USER_PICTURE,
        location = getUserLocationModel(),
    )
}