package com.exalt.domain.profile.repositories

import com.exalt.domain.profile.models.ProfileModel

interface ProfileRepository {
    suspend fun getProfile(userId: String): ProfileModel
}