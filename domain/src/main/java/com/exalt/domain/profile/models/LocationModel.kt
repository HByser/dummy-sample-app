package com.exalt.domain.profile.models

data class LocationModel(
    val street: String,
    val city: String,
    val state: String,
    val country: String,
    val timezone: String,
)