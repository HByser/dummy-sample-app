package com.exalt.domain.profile.usecases

import com.exalt.domain.profile.models.ProfileModel
import com.exalt.domain.profile.repositories.ProfileRepository
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
    private val profileRepository: ProfileRepository,
) {
    suspend fun invoke(userId: String): ProfileModel =
        profileRepository.getProfile(userId)
}