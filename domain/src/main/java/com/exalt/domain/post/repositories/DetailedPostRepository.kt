package com.exalt.domain.post.repositories

import com.exalt.domain.post.models.DetailedPostModel

interface DetailedPostRepository {
    suspend fun getDetailedPost(postId: String): DetailedPostModel
}