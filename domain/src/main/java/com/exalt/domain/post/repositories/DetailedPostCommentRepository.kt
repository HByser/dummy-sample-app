package com.exalt.domain.post.repositories

import com.exalt.domain.post.models.DetailedPostCommentModel

interface DetailedPostCommentRepository {
    suspend fun getDetailedPostComment(postId: String): List<DetailedPostCommentModel>
}