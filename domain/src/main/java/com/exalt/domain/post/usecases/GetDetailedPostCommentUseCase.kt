package com.exalt.domain.post.usecases

import com.exalt.domain.post.models.DetailedPostCommentModel
import com.exalt.domain.post.repositories.DetailedPostCommentRepository
import javax.inject.Inject

class GetDetailedPostCommentUseCase @Inject constructor(
    private val detailedPostCommentRepository: DetailedPostCommentRepository,
) {
    suspend fun invoke(postId: String): List<DetailedPostCommentModel> = runCatching {
        detailedPostCommentRepository.getDetailedPostComment(postId)
    }.getOrDefault(emptyList())
}