package com.exalt.domain.post.models

import com.exalt.domain.home.models.OwnerPreviewModel

data class DetailedPostCommentModel(
    val id: String,
    val message: String,
    val owner: OwnerPreviewModel,
    val publishDate: String,
)