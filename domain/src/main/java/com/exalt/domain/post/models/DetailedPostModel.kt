package com.exalt.domain.post.models

import com.exalt.domain.home.models.OwnerPreviewModel

data class DetailedPostModel(
    val id: String,
    val text: String,
    val image: String,
    val likes: Int,
    val tags: List<String>,
    val publishDate: String,
    val owner: OwnerPreviewModel,
)