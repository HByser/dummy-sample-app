package com.exalt.domain.post.usecases

import com.exalt.domain.post.models.DetailedPostModel
import com.exalt.domain.post.repositories.DetailedPostRepository
import javax.inject.Inject

class GetDetailedPostUseCase @Inject constructor(
    private val detailedPostRepository: DetailedPostRepository,
) {
    suspend fun invoke(postId: String): DetailedPostModel =
        detailedPostRepository.getDetailedPost(postId)
}
