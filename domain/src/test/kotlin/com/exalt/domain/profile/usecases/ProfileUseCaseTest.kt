package com.exalt.domain.profile.usecases

import com.exalt.domain.home.models.DomainModelFactory.getDefaultProfileModel
import com.exalt.domain.profile.repositories.ProfileRepository
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Test

class ProfileUseCaseTest {
    private val profileRepository: ProfileRepository = mockk()
    private val getProfileUseCase = GetProfileUseCase(profileRepository)

    @Test
    fun `Given repository returns a profile, When ProfileUserCase is invoked, Then return Profile`() =
        runTest {
            val expectedProfile = getDefaultProfileModel("1")

            coEvery { profileRepository.getProfile("1") } returns expectedProfile

            val actualProfile = getProfileUseCase.invoke("1")

            assertEquals(expectedProfile, actualProfile)
        }
}