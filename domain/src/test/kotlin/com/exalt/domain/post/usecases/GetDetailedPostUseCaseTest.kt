package com.exalt.domain.post.usecases

import com.exalt.domain.home.models.DomainModelFactory.getDetailedPostModel
import com.exalt.domain.post.repositories.DetailedPostRepository
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.Assert.assertEquals
import kotlinx.coroutines.test.runTest
import org.junit.Test

class GetDetailedPostUseCaseTest {
    private val detailedPostRepository: DetailedPostRepository = mockk()
    private val getDetailedPostUseCase = GetDetailedPostUseCase(detailedPostRepository)

    @Test
    fun `Given repository returns a single post, When DetailedPostUseCase is invoked, Then return post`() =
        runTest {
            // Given
            val expectedDetailedPost = getDetailedPostModel("1")

            coEvery { detailedPostRepository.getDetailedPost("1") } returns expectedDetailedPost

            // When
            val actualDetailedPost = getDetailedPostUseCase.invoke("1")

            // Then
            assertEquals(expectedDetailedPost, actualDetailedPost)
        }
}
