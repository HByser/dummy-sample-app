package com.exalt.domain.post.usecases

import com.exalt.domain.home.models.DomainModelFactory.getDefaultDetailedPostCommentModel
import com.exalt.domain.post.repositories.DetailedPostCommentRepository
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.test.runTest
import org.junit.Test

class GetDetailedPostCommentUseCaseTest {
    private val detailedPostCommentRepository: DetailedPostCommentRepository = mockk()
    private val getDetailedPostCommentUseCase = GetDetailedPostCommentUseCase(detailedPostCommentRepository)

    @Test
    fun `Given repository with comment list, When GetDetailedPostCommentUseCase is invoked, Then returns detailedPostComment`() = runTest {
        val expectedDetailedPostComment = List(5) {
            getDefaultDetailedPostCommentModel("1")
        }

        coEvery { detailedPostCommentRepository.getDetailedPostComment("1") } returns expectedDetailedPostComment

        val actualDetailedPostComment = getDetailedPostCommentUseCase.invoke("1")

        assertEquals(expectedDetailedPostComment, actualDetailedPostComment)
    }

    @Test
    fun `Given repository returns empty list, When GetDetailedPostCommentUseCase is invoked, Then returns empty list`() = runTest {
        coEvery { detailedPostCommentRepository.getDetailedPostComment("1") } returns emptyList()

        val detailedPostCommentList = getDetailedPostCommentUseCase.invoke("1")

        assertTrue(detailedPostCommentList.isEmpty())
    }

    @Test
    fun `Given repository throws exception, When GetDetailedPostComment is invoked, Then returns empty list`() = runTest {
        coEvery { detailedPostCommentRepository.getDetailedPostComment("1") } throws NullPointerException()

        val detailedPostCommentList = getDetailedPostCommentUseCase.invoke("1")

        assertTrue(detailedPostCommentList.isEmpty())
    }
}