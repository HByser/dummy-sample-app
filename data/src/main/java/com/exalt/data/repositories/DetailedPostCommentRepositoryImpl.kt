package com.exalt.data.repositories

import com.exalt.api.services.DetailedPostCommentService
import com.exalt.data.mappers.DetailedPostCommentMapper
import com.exalt.domain.post.models.DetailedPostCommentModel
import com.exalt.domain.post.repositories.DetailedPostCommentRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class DetailedPostCommentRepositoryImpl @Inject constructor(
    private val detailedPostCommentService: DetailedPostCommentService,
    private val detailedPostCommentMapper: DetailedPostCommentMapper,
): DetailedPostCommentRepository {
    override suspend fun getDetailedPostComment(postId: String): List<DetailedPostCommentModel> =
        detailedPostCommentService.getDetailedPostComment(postId).let { response ->
            print("response ---> $response")
            detailedPostCommentMapper.fromCommentListDto(response.body()!!.data)
        }
}