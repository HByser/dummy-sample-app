package com.exalt.data.repositories

import com.exalt.api.services.ProfileService
import com.exalt.data.mappers.ProfileMapper
import com.exalt.domain.profile.models.ProfileModel
import com.exalt.domain.profile.repositories.ProfileRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class ProfileRepositoryImpl @Inject constructor(
    private val profileService: ProfileService,
    private val profileMapper: ProfileMapper,
) : ProfileRepository {
    override suspend fun getProfile(userId: String): ProfileModel =
        profileService.getProfile(userId).let { response ->
            println("response profile ---> $response")
            profileMapper.fromDto(response.body()!!)
        }
}