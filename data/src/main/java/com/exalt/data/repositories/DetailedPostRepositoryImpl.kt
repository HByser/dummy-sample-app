package com.exalt.data.repositories

import com.exalt.api.services.DetailedPostService
import com.exalt.data.mappers.DetailedPostMapper
import com.exalt.domain.post.models.DetailedPostModel
import com.exalt.domain.post.repositories.DetailedPostRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class DetailedPostRepositoryImpl @Inject constructor(
    private val detailedPostService: DetailedPostService,
    private val detailedPostMapper: DetailedPostMapper,
) : DetailedPostRepository {
    override suspend fun getDetailedPost(postId: String): DetailedPostModel =
        detailedPostService.getDetailedPost(postId).let { response ->
            detailedPostMapper.fromDto(response.body()!!.data.find { it.id == postId })
        }
}