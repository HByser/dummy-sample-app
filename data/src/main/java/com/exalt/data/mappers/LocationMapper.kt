package com.exalt.data.mappers

import com.exalt.api.models.LocationDTO
import com.exalt.domain.profile.models.LocationModel
import javax.inject.Inject

class LocationMapper @Inject constructor() {
    fun locationFromDto(location: LocationDTO) = LocationModel(
        street = location.street,
        city = location.city,
        state = location.state,
        country = location.country,
        timezone = location.timezone,
    )
}