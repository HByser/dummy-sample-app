package com.exalt.data.mappers

import com.exalt.api.models.DetailedPostCommentDTO
import com.exalt.domain.post.models.DetailedPostCommentModel
import javax.inject.Inject

class DetailedPostCommentMapper @Inject constructor(
    private val ownerPreviewMapper: OwnerPreviewMapper,
) {
    fun fromCommentListDto(comments: List<DetailedPostCommentDTO>): List<DetailedPostCommentModel> =
        comments.map { fromCommentDto(it) }

    private fun fromCommentDto(detailedPostComment: DetailedPostCommentDTO?) = DetailedPostCommentModel(
        id = detailedPostComment!!.id,
        message = detailedPostComment.message,
        publishDate = detailedPostComment.publishDate,
        owner = ownerPreviewMapper.fromDto(detailedPostComment.owner),
    )
}