package com.exalt.data.mappers

import com.exalt.api.models.DetailedPostDTO
import com.exalt.domain.post.models.DetailedPostModel
import javax.inject.Inject

class DetailedPostMapper @Inject constructor(
    private val ownerPreviewMapper: OwnerPreviewMapper,
) {

    fun fromDto(detailedPost: DetailedPostDTO?) = DetailedPostModel(
        id = detailedPost!!.id,
        text = detailedPost.text,
        image = detailedPost.image,
        likes = detailedPost.likes,
        tags = detailedPost.tags,
        publishDate = detailedPost.publishDate,
        owner = ownerPreviewMapper.fromDto(detailedPost.owner),
    )
}