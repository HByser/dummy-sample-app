package com.exalt.data.mappers

import com.exalt.api.models.ProfileDTO
import com.exalt.domain.profile.models.ProfileModel
import javax.inject.Inject

class ProfileMapper @Inject constructor(
    private val locationMapper: LocationMapper,
) {
    fun fromDto(profileDTO: ProfileDTO) = ProfileModel(
        id = profileDTO.id,
        title = profileDTO.title,
        firstName = profileDTO.firstName,
        lastName = profileDTO.lastName,
        gender = profileDTO.gender,
        email = profileDTO.email,
        dateOfBirth = profileDTO.dateOfBirth,
        registerDate = profileDTO.registerDate,
        phone = profileDTO.phone,
        picture = profileDTO.picture,
        location = locationMapper.locationFromDto(profileDTO.location)
    )
}