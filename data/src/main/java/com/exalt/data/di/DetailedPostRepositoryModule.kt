package com.exalt.data.di

import com.exalt.data.repositories.DetailedPostRepositoryImpl
import com.exalt.domain.post.repositories.DetailedPostRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DetailedPostRepositoryModule {

    @Binds
    internal abstract fun bindDetailedPostRepository(impl: DetailedPostRepositoryImpl): DetailedPostRepository
}