package com.exalt.data.di

import com.exalt.data.repositories.DetailedPostCommentRepositoryImpl
import com.exalt.domain.post.repositories.DetailedPostCommentRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DetailedPostCommentRepositoryModule {

    @Binds
    internal abstract fun bindDetailedPostCommentRepository(impl: DetailedPostCommentRepositoryImpl): DetailedPostCommentRepository
}