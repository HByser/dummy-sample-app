package com.exalt.api

import com.exalt.api.services.DetailedPostCommentService
import com.exalt.api.services.DetailedPostService
import com.exalt.api.services.PostService
import com.exalt.api.services.ProfileService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DummyApi {
    companion object {
        const val APP_ID = BuildConfig.API_KEY
    }

    val postService: PostService
    val postDetailedService: DetailedPostService
    val postDetailedCommentService: DetailedPostCommentService
    val profileService: ProfileService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://dummyapi.io/data/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        postService = retrofit.create(PostService::class.java)
        postDetailedService = retrofit.create(DetailedPostService::class.java)
        postDetailedCommentService = retrofit.create(DetailedPostCommentService::class.java)
        profileService = retrofit.create(ProfileService::class.java)
    }

}