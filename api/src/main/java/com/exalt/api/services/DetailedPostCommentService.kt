package com.exalt.api.services

import com.exalt.api.DummyApi
import com.exalt.api.models.DetailedPostCommentDTO
import com.exalt.api.models.Page
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface DetailedPostCommentService {
    @GET("post/{postId}/comment/")
    @Headers("app-id: ${DummyApi.APP_ID}")
    suspend fun getDetailedPostComment(@Path("postId") postId: String): Response<Page<DetailedPostCommentDTO>>
}