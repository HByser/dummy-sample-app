package com.exalt.api.services

import com.exalt.api.DummyApi
import com.exalt.api.models.ProfileDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface ProfileService {
    @GET("user/{userId}")
    @Headers("app-id: ${DummyApi.APP_ID}")
    suspend fun getProfile(@Path("userId") userId: String): Response<ProfileDTO>
}