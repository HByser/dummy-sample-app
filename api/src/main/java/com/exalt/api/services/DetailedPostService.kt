package com.exalt.api.services

import com.exalt.api.DummyApi
import com.exalt.api.models.Page
import com.exalt.api.models.DetailedPostDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface DetailedPostService {
    @GET("post/")
    @Headers("app-id: ${DummyApi.APP_ID}")
    suspend fun getDetailedPost(@Query("postId") postId: String): Response<Page<DetailedPostDTO>>
}