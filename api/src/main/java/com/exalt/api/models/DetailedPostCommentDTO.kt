package com.exalt.api.models

data class DetailedPostCommentDTO(
    val id: String,
    val message: String,
    val owner: UserPreviewDTO,
    val publishDate: String,
)